# Sample-Text-on-HTML
Sample Text on HTML
<html>
  <head>
      <title>Text</title>
  </head>
<body>
      <h1>The Story in the Book</h1>
      <h2>Chapter</h2>
    <p>Molly had been staring out of her window   for about an hoyr now. On her desk, lying between copy of <i>Nature</i>, 
    <i>Now Scientist</i>, and all
     the other scientific journals her work had appeared in,
     was a well thumbed copy of <cite>On
     The Road</cite>. It had been Molly's favorite book
     since college, and the longer she spent in these
     four walls the more she left she needed to be
     free.</p>
     <p>She had spent the last ten years in this room,
      sitting under a poster with an Oscar Wilde quote
      proclaiming that <q>Work is the refuge of people
      who have nothing better to do,</q> Although
      many considered her pioneering work, unraveling
      the secrets of the llama <abbr title="Deoxyribonuclic acid">DNA </abbr>, 
      to be an outstanding achievement, Molly <em>did</em> think
      she had something better do.</p>
</body>
</html>
